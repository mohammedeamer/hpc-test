#!/bin/bash

#$ -V
#$ -N hpc-test
#$ -cwd
#$ -M <email>
#$ -m aes

source ~/anaconda3/etc/profile.d/conda.sh
conda activate dl

python run.py

# qsub example: qsub qsub_start.sh
