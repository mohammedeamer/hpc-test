import time, sys, logging

log_format = '%(asctime)s %(message)s'
logging.basicConfig(stream=sys.stdout, level=logging.INFO,
				    format=log_format, datefmt='%m/%d %I:%M:%S %p')

import torch as tch

USE_CUDA=True

if USE_CUDA:
	device = tch.device('cuda' if tch.cuda.is_available() else 'cpu')
else:
	device = tch.device('cpu')

ITERS_NUM = 10**5
N = 100

def write_result(res):
	with open('results.txt', 'w') as f:
		f.write(res)

def run():

	M = tch.eye(N).to(device)

	start_t = time.time()

	for iter_idx in range(ITERS_NUM):

		M @ M
		logging.info('iter: {}/{}'.format(iter_idx+1, ITERS_NUM))

	end_t = time.time()

	write_result(str(end_t-start_t))

if __name__ == '__main__':
	run()
